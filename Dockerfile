FROM nikolaik/python-nodejs:latest as build-stage
LABEL maintainer="Bernard-Marie <onbern@gmail.com>"
LABEL name="arsen_artefat/app_client_arsen"
LABEL version="0.0.1"
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json .
COPY package-lock.json .
RUN npm install
RUN npm i vue@latest vue-template-compiler@latest vue-server-renderer@latest
COPY . .
RUN export NODE_ENV=production
RUN npm run svg
RUN npm run generate

FROM nginx:alpine
LABEL maintainer="Bernard-Marie <onbern@gmail.com>"
LABEL name="arsen/app_client_arsen"
LABEL version="0.0.1"
#COPY nginx_config/default.conf /etc/nginx/conf.d/default.conf
COPY --from=build-stage /usr/src/app/dist /usr/share/nginx/html
