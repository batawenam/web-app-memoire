/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '011-selected': {
    width: 18,
    height: 18,
    viewBox: '0 0 18 18',
    data: '<circle pid="0" cx="9" cy="9" r="9" _fill="#2864F0"/><path pid="1" fill-rule="evenodd" clip-rule="evenodd" d="M14.67 4.545c.44.44.44 1.152 0 1.591l-5.62 5.621a.536.536 0 0 1-.759 0L5.295 8.761a1.125 1.125 0 0 1 1.591-1.59l1.785 1.783 4.409-4.409c.439-.439 1.151-.439 1.59 0z" _fill="#fff"/>'
  }
})
