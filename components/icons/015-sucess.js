/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '015-sucess': {
    width: 16,
    height: 11,
    viewBox: '0 0 16 11',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M15.397.525a.797.797 0 0 1 0 1.153L5.634 11 .604 6.196a.797.797 0 0 1 1.1-1.152l3.93 3.753L14.296.525a.797.797 0 0 1 1.1 0z" _fill="#0CD173"/>'
  }
})
