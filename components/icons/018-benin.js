/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '018-benin': {
    width: 26,
    height: 20,
    viewBox: '0 0 26 20',
    data: '<path pid="0" d="M23.111.611h-13V10h15.89V3.5A2.889 2.889 0 0 0 23.11.611z" _fill="#FCD116"/><path pid="1" d="M10.111 19.389h13a2.889 2.889 0 0 0 2.89-2.889V10H10.11v9.389z" _fill="#E8112D"/><path pid="2" d="M10.111.611H2.89A2.889 2.889 0 0 0 0 3.5v13a2.889 2.889 0 0 0 2.889 2.889h7.222V.61z" _fill="#008751"/>'
  }
})
