/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '019-resend': {
    width: 14,
    height: 14,
    viewBox: '0 0 14 14',
    data: '<path pid="0" d="M13.328 6.17a.521.521 0 1 0-1.028.174 5.741 5.741 0 1 1-1.426-2.915l-2.31.77a.522.522 0 0 0 .33.99l3.128-1.044a.522.522 0 0 0 .357-.495V.521a.522.522 0 0 0-1.043 0v1.905a6.71 6.71 0 1 0 1.992 3.745z" _fill="#2864F0"/>'
  }
})
