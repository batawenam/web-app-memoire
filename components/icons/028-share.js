/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '028-share': {
    width: 36,
    height: 36,
    viewBox: '0 0 36 36',
    data: '<g filter="url(#filter0_d)"><circle pid="0" cx="18" cy="16" r="16" _fill="#2864F0"/></g><path pid="1" d="M25.579 14.727l-6.3-6.867v4.097H17.94c-4.153 0-7.519 3.346-7.519 7.475v2.171l.595-.648a10.704 10.704 0 0 1 7.885-3.458h.379v4.097l6.299-6.867z" _fill="#fff"/><defs><filter id="filter0_d" x=".354" y="0" width="35.292" height="35.292" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB"><feFlood flood-opacity="0" result="BackgroundImageFix"/><feColorMatrix in="SourceAlpha" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"/><feOffset dy="1.646"/><feGaussianBlur stdDeviation=".823"/><feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.1 0"/><feBlend in2="BackgroundImageFix" result="effect1_dropShadow"/><feBlend in="SourceGraphic" in2="effect1_dropShadow" result="shape"/></filter></defs>'
  }
})
