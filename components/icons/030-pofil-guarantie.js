/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '030-pofil-guarantie': {
    width: 24,
    height: 24,
    viewBox: '0 0 24 24',
    data: '<path pid="0" fill-rule="evenodd" clip-rule="evenodd" d="M12 0C5.4 0 0 5.4 0 12c0 4.05 2.25 7.95 5.7 10.2C7.5 23.4 9.75 24 12 24s4.5-.6 6.3-1.8c3.45-2.1 5.7-6 5.7-10.2 0-6.6-5.4-12-12-12zm0 21c-1.65 0-3.3-.45-4.65-1.35C8.55 18.6 10.2 18 12 18c1.8 0 3.45.6 4.65 1.65-1.35.9-3 1.35-4.65 1.35zm-3-9c0-1.65 1.35-3 3-3s3 1.35 3 3-1.35 3-3 3-3-1.35-3-3zm9.9 5.7c-.15-.15-.15-.3-.3-.3-.6-.6-1.35-.9-2.1-1.35C17.4 15 18 13.5 18 12c0-3.3-2.7-6-6-6s-6 2.7-6 6c0 1.5.6 3 1.65 4.05-.75.3-1.5.75-2.1 1.35l-.3.3C3.75 16.2 3 14.1 3 12c0-4.95 4.05-9 9-9s9 4.05 9 9c0 2.1-.75 4.2-2.1 5.7z" _fill="#2864F0"/>'
  }
})
