/* eslint-disable */
var icon = require('vue-svgicon')
icon.register({
  '038-add': {
    width: 19,
    height: 19,
    viewBox: '0 0 19 19',
    data: '<path pid="0" d="M18.692 10.256h-8.388v8.424H8.72v-8.424H.368V8.744H8.72V.32h1.584v8.424h8.388v1.512z" _fill="#000"/>'
  }
})
