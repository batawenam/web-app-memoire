module.exports = {
  /*
  ** Headers of the page
  */
  router: {
    base: '/'
  },
  head: {
    title: 'Arsen | Assurance santé pour les entreprise et les indépendants',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Arsen is assurance mobile app' }
    ],
    script: [
      {src: '/driftbot.js'},

    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" ' },
      {rel: "stylesheet", href: 'https://cdn.jsdelivr.net/npm/bulma-checkradio@2.1.0/dist/css/bulma-checkradio.min.css'},
      {rel: "stylesheet", href: 'https://cdn.jsdelivr.net/npm/bulma-extensions@6.2.2/bulma-switch/dist/css/bulma-switch.min.css'},
      {rel: "stylesheet", href: 'https://cdn.jsdelivr.net/npm/bulma-tooltip@2.0.2/dist/css/bulma-tooltip.min.css'},
    ]
  },
  css: [
    {src: '~/assets/css/main.scss', lang: 'scss'},
    { src: 'font-awesome/scss/font-awesome.scss', lang: 'scss' },
  ],
  plugins: [
    {src: '~plugins/vee-validate.js', ssr: true},  // true
    {src: '~plugins/vue-svgicon.js', ssr: false},
    {src: '~/plugins/v-autocomplete.js', ssr: true}, // true
    {src: "~/plugins/vue-input-tag.js", ssr: false},
    {src: "~/plugins/vue-spiner.js", ssr:false},
    {src: "~/plugins/firebase.js", ssr: true}, // true
  ],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['axios'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },
  // variable d'environement partager coté server
  modules: [
    ['@nuxtjs/axios']  ],
/*
  metaInfo: {
    bodyScript: [
      { src: 'https://cdn.kkiapay.me/k.js',  body: true}
    ]
  }
  */
};


