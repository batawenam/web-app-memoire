export default  {
  ciExist: '/api/v1/entrance/signup/ci-cheking',
  emailValidation: '/api/v1/entrance/signup/request-email-verification',
  phoneValidation: '/api/v1/entrance/signup/request-phone-verification',
  entrepriseEmailValidation: "/api/v1/entrance/signup/request-email-verification",
  confirmPhone: '/api/v1/entrance/confirm-phone',
  confirmEmail: "/api/v1/entrance/confirm-email",
  signUpParticulier: '/api/v1/entrance/signup/particulier',
  signUpEntreprise: '/api/v1/entrance/signup/entreprise',
  login: '/api/v1/entrance/login',
  getContracts: '/api/v1/get-contracts',
  newSubscription: '/api/v1/new-subscription'
}
