import firebase from 'firebase/app'
import 'firebase/database'

if (!firebase.apps.length) {

  const config = {
    apiKey: "AIzaSyBygkm-83-vx1MYSkfjLpMdsq9PrfBIYJ8",
    authDomain: "arsen-d2b04.firebaseapp.com",
    databaseURL: "https://arsen-d2b04.firebaseio.com",
    projectId: "arsen-d2b04",
    storageBucket: "arsen-d2b04.appspot.com",
    messagingSenderId: "385078281797"
  };
  firebase.initializeApp(config);
}

const fireDb = firebase.database();

export {fireDb}
