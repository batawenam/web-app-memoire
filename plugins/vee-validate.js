import Vue from 'vue'
import VeeValidate, {Validator}from 'vee-validate'

Vue.use(VeeValidate, {
  inject: true
});

Vue.config.productionTip = false;

Validator.localize({
  en: {
    custom: {
      // the field name
      userCardId: {
        // the rules
        required: "Veillez remplir le champs il ne peut pas être vide",
        length: "Votre numéro de carte d'identité ne peut pas être inférieure à 9"
      },
      userEmail: {
        required: "Veillez remplir le champs il ne peut pas être vide",
        email: "Veillez saisir un email valide"
      },
      userPhone: {
        required: "Veillez remplir le champs il ne peut pas être vide",
        length: "Votre numéro de Téléhone ne peut pas être inférieure à 8"
      },
      password: {
        required: "Veillez entrez votre mot de passe svp",
        min: "Votre mot de passe doit avoir uen taille minimale de 6"
      },
      password_confirmation: {
        required: "Veillez confirmer le mot de passe",
        confirmed: "Les mot de passe ne sont pas identique"
      },
      entrepriseEmail: {
        required: "Veillez remplir le champs il ne peut pas être vide",
        email: "Veillez saisir un email valide"
      }

    }
  }
});
