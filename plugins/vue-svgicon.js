import Vue from 'vue'
import * as svgicon from 'vue-svgicon'
require('../components/icons/index');


Vue.use(svgicon, {
  classPrefix: 'ui',
  tagName: 'ui-svg'
});
