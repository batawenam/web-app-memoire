export const state = () => ({
  userInfo: null,
  userContracts: null
});

export const mutations = {
  setInfo(state, value) {
    state.userInfo = value
  },
  setContracts(state, value) {
    state.userContracts = value
  }
};
